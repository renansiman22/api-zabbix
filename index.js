console.log(login());

/*
function choose_request(option) {
    switch (option) {
        case 1:
        case 2:
        //...
    }

}
*/

// Consultar grupos
function group(group_name, login_key) {
    console.log(group_name)
    console.log(login_key)
    console.log(hostgroup_request_data(group_name, login_key))
    let groupCall = $.ajax({
        ...default_get_request,
        data: JSON.stringify(hostgroup_request_data(group_name, login_key))
    }).done((response) => {
        console.log(response);
    });
}

function login() {
    let groupName = 'Shell'
    let loginCall = $.ajax({
        ...default_post_request,
        data: JSON.stringify(login_request_data())
    }).done((response1) => {
        group(groupName, response1.result);
    })/*.then((response2) => {
        allhosts_perclient(response2.result[0], response1.result)
    });*/
}

// Consultar templates
function template_ID(temp_name, login_key) {
    let templateIdCall = $.ajax({
        ...default_get_request,
        data: JSON.stringify(template_request_data(temp_name, login_key))
    });

    templateIdCall.done(function (response) {
        console.log(response.result);
    });
}

// Consultar hosts
function host(group_id, temp_id, host_name, login_key) {
    let hostCall = $.ajax({
        ...default_get_request,
        data: JSON.stringify(group_id, temp_id, host_name, login_key)
    });

    hostCall.done(function (response) {
        return response.result[0];
    });
}

// Consultar itens
function item(host_id, item_key, login_key) {
    let itemCall = $.ajax({
        ...default_get_request,
        data: item_request_data(host_id, item_key, login_key)
    });

    itemCall.done(function (response) {
        return response.result;
    });
}

// Solicitar histórico com base na ID do item
function history(item_id, limit_value, login_key) {
    let historyCall = $.ajax({
        ...default_get_request,
        data: history_request_data(item_id, limit_value, login_key)
    });

    historyCall.done(function (response) {
        return response.result;
    });
}

// Solicitar todos dispositivos de um determinado cliente
function allhosts_perclient(group_id, login_key) {
    let allhost_call = $.ajax({
        ...default_get_request,
        data: allhostsperclient_request_data(group_id, login_key)
    });

    allhost_call.done(function (response) {
        return response.result;
    })
}

/*
// Tabela de links
function draw_links_table(temp_name) {
    let hostids = template_ID(temp_name);

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Nome servidor');
    data.addColumn('boolean', 'Status Atual');
    data.addRows([

        ['Mike', true],
        ['Jim', false],
        ['Alice', true],
        ['Bob', true]
    ]);

    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, { showRowNumber: true, width: '50%', height: '20%' });

}

//Função uptime/downtime
function up_downtime(group, template, device) {


}

//Gráfico de uptime/downtime
function up_downtime_graph() {
    google.charts.load('current', { 'packages': ['timeline'] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var container = document.getElementById('timeline');
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({ type: 'string', id: 'President' });
        dataTable.addColumn({ type: 'date', id: 'Start' });
        dataTable.addColumn({ type: 'date', id: 'End' });
        dataTable.addRows([
            ['Washington', new Date(1789, 3, 30), new Date(1797, 2, 4)],
            ['Adams', new Date(1797, 2, 4), new Date(1801, 2, 4)],
            ['Jefferson', new Date(1801, 2, 4), new Date(1809, 2, 4)]]);

        chart.draw(dataTable);
    }
}

 //Gráfico de memórias
 function memory_graph() {
    google.charts.load('current', { 'packages': ['gauge'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['Memory', 80],
            ['CPU', 55],
            ['Network', 68]
        ]);

        var options = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom: 75, yellowTo: 90,
            minorTicks: 5
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

        setInterval(function () {
            data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
            chart.draw(data, options);
        }, 13000);
        setInterval(function () {
            data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
            chart.draw(data, options);
        }, 5000);
        setInterval(function () {
            data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
            chart.draw(data, options);
        }, 26000);
    }
}
*/