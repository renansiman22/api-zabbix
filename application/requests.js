const zabbix_url = 'http://10.150.45.138:8082/api_jsonrpc.php';

const default_request = {
    type: 'POST',
    url: zabbix_url,
    contentType: "application/json",
    dataType: 'json'
}

const login_request_data = () => {
    return {
        jsonrpc: '2.0',
        method: 'user.login',
        params: {
            user: 'Admin',
            password: 'zabbix'
        },
        id: 1
    }
}

const hostgroup_request_data = (group_name, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'hostgroup.get',
        params: {
            output: 'extend',
            filter: {
                name: group_name
            }
        },
        auth: login_key,
        id: 1
    };
}

const template_request_data = (temp_name, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'template.get',
        params: {
            output: 'extend',
            filter: {
                name: temp_name
            }
        },
        auth: login_key,
        id: 1
    };
}

const host_request_data = (group_id, temp_id, host_name, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'host.get',
        params: {
            output: 'extend',
            groupids: group_id,
            templateids: temp_id,
            filter: {
                name: host_name
            }
        },
        auth: login_key,
        id: 1
    }
}

const item_request_data = (host_id, item_key, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'item.get',
        params: {
            output: 'extend',
            hostids: host_id,
            filter: {
                key_: item_key
            }
        },
        auth: login_key,
        id: 1
    }
}

const history_request_data = (item_id, limit_value, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'history.get',
        params: {
            output: 'extend',
            history: 3,
            sortfield: 'clock',
            sortorder: 'DESC',
            itemids: item_id,
            limit: limit_value
        },
        auth: login_key,
        id: 1
    }
}

//busca dispositivos de um determinado cliente
const allhostsperclient_request_data = (group_id, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'host.get',
        params: {
            output: 'extend',
            groupids: group_id
        },
        auth: login_key,
        id: 1
    }
}

//busca dispositivos de um determinado template de um cliente
const allhostspertemplate_request_data = (temp_id, group_id, login_key) => {
    return {
        jsonrpc: '2.0',
        method: 'host.get',
        params: {
            output: 'extend',
            templateids: temp_id,
            groupids: group_id
        },
        auth: login_key,
        id: 1
    }
}