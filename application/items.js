var global_response;

function login() {

    let loginCall = $.ajax({
        ...default_request,
        data: JSON.stringify(login_request_data())
    }).done((response) => {
        host(group_id, host_name, response.result);
    });
}

// Consultar hosts
function host(group_id, temp_id, host_name, login_key) {
    let hostCall = $.ajax({
        ...default_request,
        data: JSON.stringify(group_id, temp_id, host_name, login_key)
    }).done((response) => {
        item(response.result[0].hostid, item_key, login_key);
    });
}

// Consultar itens
function item(host_id, item_key, login_key) {
    let itemCall = $.ajax({
        ...default_get_request,
        data: item_request_data(host_id, item_key, login_key)
    }).done(function (response) {
        history(response.result[0].itemid, 1, login_key);
    });
}

// Solicitar histórico com base na ID do item
function history(item_id, limit_value, login_key) {
    let historyCall = $.ajax({
        ...default_request,
        data: history_request_data(item_id, limit_value, login_key)
    });

    historyCall.done(function (response) {
        return response.result;
    });
}

function drawChart() {

    let historyByItem = [];

   /* for (i = 0; i < global_response.result.length; i++) {
        historyByItem[i] = global_response.result[i].clock;
    }*/

    var container = document.getElementById('example2.2');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'string', id: 'Term' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });
    dataTable.addRows([""]);

    var options = {
        timeline: { showRowLabels: false }
    };

    chart.draw(dataTable, options);
}