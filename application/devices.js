console.log(login());

var temp_name = "Sensor"

function login() {
    
    let loginCall = $.ajax({
        ...default_request,
        data: JSON.stringify(login_request_data())
    }).done((response) => {
        template_ID(temp_name, response.result);
    }).then((response) => {
        allhosts_perclient("23", response.result);
    });
}

// Solicitar todos dispositivos de um determinado cliente
function allhosts_perclient(group_id, login_key) {
    let allhost_call = $.ajax({
        ...default_request,
        data: JSON.stringify(allhostsperclient_request_data(group_id, login_key))
    }).done((response) => {
        console.log(response);
    })
}

// Consultar templates
function template_ID(temp_name, login_key) {
    let templateIdCall = $.ajax({
        ...default_request,
        data: JSON.stringify(template_request_data(temp_name, login_key))
    });

    templateIdCall.done(function (response) {
        console.log(response.result[0].templateid);
    });
}

// Solicitar todos dispositivos de um determinado template de um cliente
function allhosts_pertemplate(temp_id, group_id, login_key) {
    let allhost_call = $.ajax({
        ...default_get_request,
        data: allhostspertemplate_request_data(temp_id, group_id, login_key)
    });

    allhost_call.done(function (response) {
        return response.result;
    })
}