console.log(login());

var CLIENTS = ["BASF", "BBA", "Iconic", "Prysmian", "Shell", "Ypê"]
var global_response;

function login() {
    let loginCall = $.ajax({
        ...default_request,
        data: JSON.stringify(login_request_data())
    }).done((response) => {
        group(CLIENTS, response.result);
    })
}

// Solicitar grupos de clientes
function group(group_name, login_key) {
    console.log(group_name)
    console.log(login_key)
    console.log(hostgroup_request_data(group_name, login_key))
    let groupCall = $.ajax({
        ...default_request,
        data: JSON.stringify(hostgroup_request_data(group_name, login_key))
    }).done((response) => {
        global_response = response;
    });
}

function value_return(value_id){
    return value_id;
}

function drawTable() {
    let clients = [];
    
    for (let i = 0; i < global_response.result.length; i++) {
        clients.push([global_response.result[i].groupid, global_response.result[i].name]);
    }

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'ID');
    data.addColumn('string', 'Clientes');
    data.addRows(clients);
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, { showRowNumber: false, width: '70%', height: '30%' });

    let option = "23";
    value_return(option);
    
}